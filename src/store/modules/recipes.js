import * as types from '../mutation-types'
import api from '../../api/recipes'

// initial state
const state = {
    nombre: '',
    descripcion: '',
    imagen: ''
};

// getters
const getters = {
    nombre: state => state.nombre,
    descripcion: state => state.descripcion,
    imagen: state => state.imagen
};

// actions
const actions = {
    [types.GET_RECIPE](store, id) {

        store.commit(types.CLEAR_RECIPE);

        api.getRecipe(id, (recipe) => {
            store.commit(types.SET_RECIPE, recipe);
        });
    }
};

const mutations = {
    [types.SET_RECIPE] (state, recipe) {
        state.nombre = recipe.name;
        state.descripcion = recipe.description;
        state.imagen = recipe.image_url;
    },

    [types.CLEAR_RECIPE] (state, recipe) {
        state.nombre = '';
        state.descripcion = '';
        state.imagen = '';
    }
};

export default {
    state,
    getters,
    actions,
    mutations
}
