import Vue from 'vue'
import VueResource from 'vue-resource'

Vue.use(VueResource);

export default {
    getRecipe(id, callback) {
        let url = 'https://api.punkapi.com/v2/beers/' + id;

        Vue.http.get(url).then(
            response => {
                callback(response.body[0]);
            },
            error => {
                alert("No existe la " + id);
            }
        );
    }
}
